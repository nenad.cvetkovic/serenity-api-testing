package serenity.model;

import lombok.Getter;

/*
{"author":null,
"title":"Germany backtracks on virus lockdown for Easter",
"description":"",
"url":"http:\/\/rss.cnn.com\/~r\/rss\/cnn_topstories\/~3\/J0g7QN4MbaI\/h_487c2b65be70b5439183b9a2de158adc",
"source":"CNN",
"image":"https:\/\/cdn.cnn.com\/cnnnext\/dam\/assets\/150325082152-social-gfx-cnn-logo-super-169.jpg",
"category":"general",
"language":"en",
"country":"us",
"published_at":"2021-03-24T13:00:04+00:00"}

 */

@Getter
public class NewsDetails {
    private String author;
    private String title;
    private String description;
    private String url;
    private String source;
    private String image;
    private String category;
    private String language;
    private String country;
    private String published_at;
}
