package serenity.parameters;

public enum Categories implements QueryParameter {
    ADD_GENERAL,
    NOT_GENERAL,
    ADD_BUSINESS,
    NOT_BUSINESS,
    ADD_SPORTS,
    NOT_SPORTS,
    ADD_ENTERTAINMENT,
    NOT_ENTERTAINMENT,
    ADD_HEALTH,
    NOT_HEALTH,
    ADD_SCIENCE,
    NOT_SCIENCE,
    ADD_TECHNOLOGY,
    NOT_TECHNOLOGY;

    @Override
    public String parameter() {
        return "&categories=";
    }

    @Override
    public String process() {
        return this.toString().toLowerCase().replace("add_", "").replace("not_", "-");
    }

}
