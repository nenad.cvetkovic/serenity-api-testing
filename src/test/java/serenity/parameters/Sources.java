package serenity.parameters;

public enum Sources implements QueryParameter {
    ADD_BBC,
    NOT_BBC,
    ADD_CNN,
    NOT_CNN;

    @Override
    public String parameter() {
        return "&sources=";
    }

    @Override
    public String process(){
        return this.toString().toLowerCase().replace("add_", "").replace("not_", "-");
    }
}
