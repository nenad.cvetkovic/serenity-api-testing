package serenity.parameters;

public interface QueryParameter {
    String parameter();

    String process();
}
