package serenity.parameters;

public enum Languages implements QueryParameter {
    ADD_EN,
    NOT_EN,
    ADD_DE,
    NOT_DE;

    @Override
    public String parameter() {
        return "&languages=";
    }

    @Override
    public String process() {
        return this.toString().toLowerCase().replace("add_", "").replace("not_", "-");
    }
}
