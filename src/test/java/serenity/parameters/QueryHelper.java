package serenity.parameters;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class QueryHelper {

    public static String setKeywords(String... keywords) {
        return getString("&keywords=", keywords);
    }

    public static String setLimit(int limit) {
        return "&limit=" + limit;
    }

    public static String setSources(QueryParameter... parameters) {
        return getString(parameters);
    }

    public static String setCategories(QueryParameter... parameters) {
        return getString(parameters);
    }

    @NotNull
    private static String getString(String q, String... keywords) {
        List<String> keys = Arrays.asList(keywords);

        return q + String.join(",", keys)
                .toLowerCase();
    }

    @NotNull
    private static String getString(QueryParameter... keywords) {
        List<QueryParameter> keys = Arrays.asList(keywords);

        String q = keys.get(0).parameter();

        return q + keys.stream()
                .map(QueryParameter::process)
                .collect(Collectors.joining(","));

    }

}
