package serenity.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.util.EnvironmentVariables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import serenity.model.NewsDetails;
import serenity.parameters.Categories;
import serenity.parameters.Sources;

import java.util.List;

import static io.restassured.internal.path.json.JsonPrettifier.prettifyJson;
import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.IsEqual.equalTo;
import static serenity.parameters.QueryHelper.*;

@RunWith(SerenityRunner.class)
public class OpenNewsDetailsTest {
    private static final Logger log = LoggerFactory.getLogger(OpenNewsDetailsTest.class);
    private static final int LIMIT = 5;
    private EnvironmentVariables environmentVariables;
    private String baseUrl;
    private Actor john;
    private String accessKey;


    @Before
    public void johnCanOpenApi() {
        // Live News Data  http://api.mediastack.com/v1/news?access_key=YOUR_ACCESS_KEY
        baseUrl = environmentVariables.getProperty("baseurl");
        accessKey = environmentVariables.getProperty("accesskey");
    }

    @Test
    public void showAllNewsForCategoryGeneral() {
        // The structure of query parameters:
        // &limit=5&keywords=virus&sources=bbc,cnn&categories=general

        String fullQuery = setLimit(LIMIT)
                + setKeywords("virus")
                + setSources(Sources.ADD_BBC, Sources.ADD_CNN)
                + setCategories(Categories.ADD_GENERAL);

        String fullUrl = baseUrl + "?access_key=" + accessKey + fullQuery;
        log.info("URL: " + fullUrl);

        john = Actor.named("John").whoCan(CallAnApi.at(fullUrl));
        john.attemptsTo(Get.resource(fullUrl));
        log.info(prettifyJson(lastResponse().asString()));

        john.should(
                seeThatResponse("News list should be populated",
                        validatableResponse -> {
                            validatableResponse.statusCode(200);
                            validatableResponse.body("pagination.limit", equalTo(LIMIT));
                            validatableResponse.body("pagination.count", greaterThan(0));
                        }
                )
        );

        List<NewsDetails> newsDetails = lastResponse().jsonPath()
                .getList("data", NewsDetails.class);

        john.attemptsTo(
                Ensure.that(newsDetails).isNotEmpty(),
                Ensure.that(newsDetails.get(0).getTitle()).isNotBlank(),
                Ensure.that(newsDetails.get(0).getUrl()).isNotBlank(),
                Ensure.that(newsDetails.get(0).getSource()).isNotBlank(),
                Ensure.that(newsDetails.get(0).getCategory()).isNotBlank(),
                Ensure.that(newsDetails.get(0).getLanguage()).isNotBlank(),
                Ensure.that(newsDetails.get(0).getCountry()).isNotBlank(),
                Ensure.that(newsDetails.get(0).getPublished_at()).isNotBlank()
        );
    }

    @Test
    public void testCallWithNonExistentCategory() {
        String limit = setLimit(LIMIT);
        String fullQuery = limit + "&categories=long_life";

        String fullUrl = baseUrl + "?access_key=" + accessKey + fullQuery;
        log.info("URL: " + fullUrl);

        john = Actor.named("John").whoCan(CallAnApi.at(fullUrl));
        john.attemptsTo(Get.resource(fullUrl));

        log.info(prettifyJson(lastResponse().asString()));

        john.should(
                seeThatResponse("Category is not valid",
                        validatableResponse -> {
                            validatableResponse.statusCode(422); //422 Unprocessable Entity
                            validatableResponse.body("error.code", equalTo("validation_error"));
                            validatableResponse.body("error.message", equalTo("Request failed with validation error"));
                            validatableResponse.body("error.context.'categories[0]'[0]", equalTo("The value you selected is not a valid choice."));
                        }
                )
        );
    }
}
