# Serenity Api Testing

Test framework based on Serenity (http://www.thucydides.info) for testing API
https://serenity-bdd.github.io/theserenitybook/latest/serenity-screenplay-rest.html


### Live News Data

http://api.mediastack.com/v1/news?access_key=YOUR_ACCESS_KEY

####optional parameters:

    & sources = cnn,bbc
    & categories = business,sports
    & countries = us,au
    & languages = en,-de
    & keywords = virus,-corona
    & sort = published_desc
    & offset = 0
    & limit = 100


### Historical News Data

http://api.mediastack.com/v1/news?access_key=YOUR_ACCESS_KEY&date=2020-02-19

#### optional parameters:
    & sources = cnn,bbc
    & categories = business,sports
    & countries = us,au
    & languages = en,-de
    & keywords = virus,-corona
    & sort = published_desc
    & offset = 0
    & limit = 100

### News Sources

http://api.mediastack.com/v1/sources?access_key=YOUR_ACCESS_KEY&search=abc

#### optional parameters:

    & categories = business,sports
    & countries = us,au
    & languages = en,-de
    & offset = 0
    & limit = 100

# Running in CI

For the purpose of running in CI, there is `Jenkinsfile` in the root of the project. This file describes all steps necessary for successful execution of the test cases. There is only one stage which consists of `steps` and `post` actions. Steps include checkout from GitLab and execution of maven command.
Post actions consist of zipping report directory and clean up of workspace.

Jenkins uses standard predefined plugins. Only plugin which was installed additionally is "Plugin Utilities API Plugin".

"Open Blue Ocean" is used for creation of pipeline. `Jenkinsfile` is firstly checked out from repository and then executed.

The artifact which can be downloaded is report in zip format. Unpack it and open `index.html` with some browser.



# Running tests locally

To run tests locally it is enough to execute next command: `mvn clean verify` . The report is generated in directory `site`. You will be able to see the exact location of `index.html` file at the end of execution.

#Tests

Only two tests are implemented. One for happy path which calls endpoint with expected parameters and which verifies success of the call, and some part of inner structure. Second tests is negative test in which non-existent category was sent to endpoint. The intention of the tests is clear form the code.


**NOTE: This was tested with local docker installation of Jenkins, and it was working as expected.**